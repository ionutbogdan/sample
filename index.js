var ejs = require('ejs');
var wkhtmltopdf = require('wkhtmltopdf');
var path = require('path');
var fs  = require('fs');
var MemoryStream = require('memorystream');
var AWS = require('aws-sdk');


process.env['PATH'] =
    process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'];

exports.handler = async (event, context) => {

    console.log(event);

    const templateName = event.template,
          language = event.language || 'de',
          type = event.type || 'email',
          partner_id = event.partnerId || 1377345,
          vars = event.vars,
          orderNumber = event.vars.bookingDetails.booking.orderNumber;

    const templateBody = await getTempalte(templateName, language, vars);
    let   partials = null,
          template = null;

    switch (type) {
        case 'email':
            partials = await getHeaderFooter(type, partner_id);
            template = partials.header + templateBody + partials.footer;
            return await generateEmailTemplate(template, templateName, orderNumber);
            break;
        case 'pdf':
            partials = await getHeaderFooter(type, partner_id);
            template = partials.header + templateBody + partials.footer;
            return await generatePdf(template, templateName, orderNumber);
            break;
        case 'calendar_reminder':
            return await generateVCalendarTemplate(templateBody, templateName, orderNumber);
            break;
    }

}

const round = (value) => {
    return parseFloat(value).toFixed(2);
};

const plural = (singular, plural = singular, count = 0) => {
    if(!singular) {
        return ''
    }
    if(count > 1) {
        return plural
    } else {
        return singular
    }
};

const dateFormat = (format, data = null) => {
    let now = null, options = null, year = null, month = null, day = null;
    switch (format) {
        case 'generateToday':
            now = new Date();
            options = {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
            };
            return now.toLocaleString('de-de', options);
            break;
        case 'ics':
            console.log(data);
            now = new Date(data);
            year = now.getFullYear().toString();
            month = (now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : now.getMonth() + 1;
            day = now.getDate() < 10 ? '0' + now.getDate() : now.getDate();
            return year + month + day;
            break;
    }
}

const getTempalte = async (templateName, language, vars) => {

    if(!templateName) {
        throw new Error('No template name given. Function does not know which template to get.')
    }

    const body = await getS3Templates('templates/template_body/'+language+'/'+templateName+'.html');
    console.log(body);

    const templateBody = ejs.render(body, {vars: vars, plural: plural, dateFormat: dateFormat, round: round});

    return templateBody
};

const getHeaderFooter = async (type, partner_id) => {
    if(!type) {
        type = 'email'
    }
    if(!partner_id) {
        partner_id = 1377345
    }


    const headerTpl = await getS3Templates('templates/template_partials/'+partner_id+'/header_'+type+'.html'),
          footerTpl = await getS3Templates('templates/template_partials/'+partner_id+'/footer_'+type+'.html');

    return {
        header: headerTpl,
        footer: footerTpl
    }

}

const generatePdf = async (template, fileName, orderNumber) => {

    const memoryStream = new MemoryStream();

    return new Promise(function(resolve, reject) {
        wkhtmltopdf(template, {
            pageSize: 'letter'
        }, async function(err, stream) {

            if(err) {
                reject(err)
            }

            let pdfFile = await memoryStream.read();

            let response = null;

            const s3Key = orderNumber + '/' + fileName + '_' + (new Date()).getTime() + '.pdf',
                  file = fileName + '_' + (new Date()).getTime() + '.pdf',
                s3Params = {
                    Bucket: "travador-dev",
                    Key: s3Key,
                    Body: pdfFile
                },
                s3 = new AWS.S3();

           await s3.putObject(s3Params, function (err, data) {
                if(err) {
                    throw new Error('Problems saving pdf in bucket: ' + err)
                }

                response = { 'attachment': {
                        bucket: s3Params.Bucket,
                        path: s3Params.Key,
                        file: file,
                    }
                }

            }).promise();

            resolve(response)

        }).pipe(memoryStream)
    })
}

const getS3Templates = async (key) => {
    const s3 = new AWS.S3();
    return new Promise((resolve, reject) => {
        s3.getObject({Bucket: "travador-dev", Key: key}, function (err, data) {
            if (err) {
                reject('Problems getting template from bucket: ' + err)
            }

            resolve(data.Body.toString('utf8'))
        })
    })
}

const generateEmailTemplate = async (template, fileName, orderNumber) => {

    let response = null

    const s3Key = orderNumber + '/' + fileName + '_' + (new Date()).getTime() + '.html',
          file = fileName + '_' + (new Date()).getTime() + '.html',
        s3Params = {
            Bucket: "travador-dev",
            Key: s3Key,
            Body: template
        },
        s3 = new AWS.S3(),
        subject = (fileName === 'confirmation_mail_b2c') ? 'Booking Confirmation #' + orderNumber : 'Payment received #' + orderNumber

    await s3.putObject(s3Params, function (err, data) {
        if(err) {
            throw new Error('Problems saving template in bucket: ' + err)
        }

        response = { 'template': {
                bucket: s3Params.Bucket,
                path: s3Params.Key,
                subject: subject,
                body: file
            }
        }

    }).promise()

    return response
}

const generateVCalendarTemplate = async (template, templateName, orderNumber) => {
    let response = null;
    const s3Key = orderNumber + '/'+ templateName +'.ics',
          s3Params = {
               Bucket: "travador-dev",
               Key: s3Key,
               Body: template
          },
          s3 = new AWS.S3();

    await s3.putObject(s3Params, function(err, data) {
        if(err) {
            throw new Error('Calendar reminder .ics file not saved: ' + err)
        }
        response = {
            attachment: {
                bucket: s3Params.Bucket,
                path: s3Params.Key,
                file: templateName + '.ics',
            }
        }
    }).promise();

    return response
}

